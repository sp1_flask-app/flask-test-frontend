#!/usr/bin/env python3
from flask import Flask, render_template, flash, jsonify, request
import requests
import json

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/search")
def search():
    q = request.args.get("q").lower()
    url = "http://backend:5000/search?q={}".format(q)
    words = requests.post(url)
    words_list = []
    for word in words.json():
        words_list.append(word)

    return render_template("index.html", words=words_list)

if __name__ == "__main__":
    app.run(debug=True, host='0.0.0.0', port=5005)